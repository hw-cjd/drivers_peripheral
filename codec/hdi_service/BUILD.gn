# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import("//build/ohos.gni")

config("headers_config") {
  include_dirs = [
    "../interfaces/include",
    "../hal/v1.0/share_mem/include",
    "./codec_service_stub",
    "./codec_proxy",
  ]
}

ohos_shared_library("libcodec_client") {
  include_dirs = [ "./common" ]

  public_configs = [ ":headers_config" ]

  sources = [
    "../hal/v1.0/share_mem/src/ashmem_wrapper.cpp",
    "../hal/v1.0/share_mem/src/share_mem.c",
    "codec_proxy/codec_proxy.c",
    "codec_proxy/proxy_msgproc.c",
    "codec_service_stub/codec_callback_service.c",
    "codec_service_stub/codec_callback_stub.c",
    "codec_service_stub/stub_msgproc.c",
    "common/common_msgproc.c",
  ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "drivers_peripheral_display:hdi_gralloc_client",
      "graphic_surface:buffer_handle",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hilog:libhilog",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
}

group("codec_client") {
  deps = [ ":libcodec_client" ]
}

ohos_shared_library("libcodec_server") {
  include_dirs = [
    "../interfaces/include",
    "./codec_proxy",
    "./codec_service_stub",
    "./common/",
    "../hal/v1.0/codec_instance/include",
    "../hal/v1.0/share_mem/include",
    "../hal/v1.0/oem_interface/include",
    "../hal/v1.0/buffer_manager/include",
  ]
  sources = [
    "../hal/v1.0/buffer_manager/src/buffer_manager.cpp",
    "../hal/v1.0/buffer_manager/src/buffer_manager_wrapper.cpp",
    "../hal/v1.0/codec_instance/src/codec_instance.c",
    "../hal/v1.0/codec_instance/src/codec_instance_manager.cpp",
    "../hal/v1.0/share_mem/src/ashmem_wrapper.cpp",
    "../hal/v1.0/share_mem/src/share_mem.c",
    "codec_proxy/codec_callback_proxy.c",
    "codec_proxy/proxy_msgproc.c",
    "codec_service_stub/codec_config_parser.c",
    "codec_service_stub/codec_host.c",
    "codec_service_stub/codec_service.c",
    "codec_service_stub/codec_stub.c",
    "common/common_msgproc.c",
  ]

  deps = [ ":codec_client" ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "drivers_peripheral_display:hdi_gralloc_client",
      "graphic_surface:buffer_handle",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hilog:libhilog",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  shlib_type = "hdi"
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
}

ohos_static_library("libcodec_server_static") {
  include_dirs = [
    "../interfaces/include",
    "../hdi_service/codec_proxy",
    "../hdi_service/common/",
    "../hal/v1.0/codec_instance/include",
    "../hal/v1.0/share_mem/include",
    "../hal/v1.0/oem_interface/include",
    "../hal/v1.0/buffer_manager/include",
  ]
  sources = [
    "../hal/v1.0/codec_instance/src/codec_instance.c",
    "../hal/v1.0/codec_instance/src/codec_instance_manager.cpp",
    "../hal/v1.0/share_mem/src/ashmem_wrapper.cpp",
    "../hal/v1.0/share_mem/src/share_mem.c",
    "codec_service_stub/codec_callback_service.c",
    "codec_service_stub/codec_callback_stub.c",
    "codec_service_stub/codec_config_parser.c",
    "codec_service_stub/codec_host.c",
    "codec_service_stub/codec_service.c",
    "codec_service_stub/codec_stub.c",
    "codec_service_stub/stub_msgproc.c",
    "common/common_msgproc.c",
  ]

  deps = [ ":codec_client" ]

  if (is_standard_system) {
    external_deps = [
      "c_utils:utils",
      "drivers_peripheral_display:hdi_gralloc_client",
      "graphic_surface:buffer_handle",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_utils",
      "hilog:libhilog",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
}

group("codec_service") {
  deps = [ ":libcodec_server" ]
}
